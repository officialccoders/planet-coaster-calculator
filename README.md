# Planet Coaster Calculator in Leptos

It is a small web app that was created to test Leptos. 

It can be used to calculate the ticket price of a ride in planet coaster using its prestige by dividing the prestige by 38.

## Usage

[Follow the instructions to install Leptos](https://book.leptos.dev/getting_started/index.html)

```
git clone https://gitlab.com/officialccoders/planet-coaster-calculator.git
cd planet-coaster-calculator
sudo cargo leptos watch
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
