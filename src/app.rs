use leptos::*;
use leptos_meta::*;
use leptos_router::*;

#[component]
pub fn App() -> impl IntoView {
    // Provides context that manages stylesheets, titles, meta tags, etc.
    provide_meta_context();

    view! {
        <Stylesheet id="leptos" href="/pkg/planet-coaster-calculator.css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins"/>

        // sets the document title
        <Title text="Welcome to Leptos"/>

        // content for this welcome page
        <Router>
            <main>
                <Routes>
                    <Route path="" view=HomePage/>
                    <Route path="/*any" view=NotFound/>
                </Routes>
            </main>
        </Router>
    }
}

/// Renders the home page of your application.
#[component]
fn HomePage() -> impl IntoView {
    view! {
        <div class="top_bar">
            <h1>"The Planet Coaster Calculator"</h1>
        </div>
        <div>{Calculator()} <h1>"Quick Table"</h1> <ViewTable initial_size=40/></div>
    }
}

#[component]
fn Calculator() -> impl IntoView {
    let (ticket, set_ticket) = create_signal(0);
    let (ticket_price, set_ticket_price) = create_signal(0);
    let (ticket_time_price, set_ticket_time_price) = create_signal(String::new());
    let (time, set_time) = create_signal(0);

    create_effect(move |_| {
        if ticket() > 0 {
            let price: i32 = ticket.get() / 38;
            set_ticket_price(price);
            if time() > 0 {
                set_ticket_time_price(format!("${}/min", price / time()));
            } else {
                set_ticket_time_price(String::new());
            }
        }
    });

    view! {
        <div>
            <form>
                <table class="input_table">
                    <tr>
                        <td>"Prestige"</td>
                        <td>
                            <input
                                type="number"
                                name="ticket"
                                on:input=move |ev| {
                                    if let Ok(value) = event_target_value(&ev).parse::<i32>() {
                                        set_ticket(value)
                                    }
                                }

                                prop:value=ticket
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>"Prestige"</td>
                        <td>
                            <input
                                type="number"
                                name="time"
                                on:input=move |ev| {
                                    if let Ok(value) = event_target_value(&ev).parse::<i32>() {
                                        set_time(value)
                                    }
                                }

                                prop:value=time
                            />
                        </td>
                    </tr>
                </table>
            </form>
            <h2>"The Ticket Price: "</h2>
            <h1 class="price">"$" {ticket_price}</h1>
            <h2>{ticket_time_price}</h2>
        </div>
    }
}

#[component]
fn ViewTable(initial_size: i32) -> impl IntoView {
    let values: Vec<Vec<i32>> = (1..=(initial_size / 5) + 1)
        .into_iter()
        .map(|i| (i - 1) * 5..i * 5)
        .map(|range| range.map(|x| x).collect())
        .collect();

    view! {
        <div>
            <table>

                {values
                    .into_iter()
                    .map(|row| {
                        if row[4] != 4 {
                            view! {
                                <tr>
                                    <td>{row[0] * 38} " = $" {row[0]}</td>
                                    <td>{row[1] * 38} " = $" {row[1]}</td>
                                    <td>{row[2] * 38} " = $" {row[2]}</td>
                                    <td>{row[3] * 38} " = $" {row[3]}</td>
                                    <td>{row[4] * 38} " = $" {row[4]}</td>
                                </tr>
                            }
                        } else {
                            view! { <tr></tr> }
                        }
                    })
                    .collect_view()}

            </table>
        </div>
    }
}

/// 404 - Not Found
#[component]
fn NotFound() -> impl IntoView {
    // set an HTTP status code 404
    // this is feature gated because it can only be done during
    // initial server-side rendering
    // if you navigate to the 404 page subsequently, the status
    // code will not be set because there is not a new HTTP request
    // to the server
    #[cfg(feature = "ssr")]
    {
        // this can be done inline because it's synchronous
        // if it were async, we'd use a server function
        let resp = expect_context::<leptos_actix::ResponseOptions>();
        resp.set_status(actix_web::http::StatusCode::NOT_FOUND);
    }

    view! { <h1>"Not Found"</h1> }
}
